// users
{
	"_id": "User001"
	"firstName": "Jane",
	"lastName" : "Doe",
	"email" : "jane@mail.com",
	"password": "jane123",
	"isAdmin": "false",
	"mobileNumber": "09123455"
}
// Products
{
	"_id" : "Product001",
	"productName" : "Pizza",
	"productDesciption": "Delicious Pizza",
	"productPrice": 70,
	"stocks": 10,
	"isActive": true,
	"SKU": "12DF92G73"
},
{
	"_id" : "Product002",
	"productName" : "Fries",
	"productDesciption": "Delicious Fries",
	"productPrice": 50,
	"stocks": 10,
	"isActive": true,
	"SKU": "12DF92G73"
}
// Order Products
{
	"_id": "orderprod001",
	"orderID": "OrderID001",
	"productID": "Product001",
	"quantity": 5,
	"orderPrice": 70,
	"subTotal": (quantity*orderPrice)
}
// orders
{
	"_id": "orders001",
	"transactionDate": "2022-06-10T15:00:00.00Z",
	"status": "active",
	"total": total + subTotal
}